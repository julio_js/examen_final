﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial
{
    class Empleado
    {
        private int codigo;
        private DateTime fecha;
        private float subtotal;
        private float iva;
        private float total;

        public Empleado(int codigo, DateTime fecha, float subtotal, float iva, float total)
        {
            this.codigo = codigo;
            this.fecha = fecha;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
        }

        public int Codigo
        {
            get
            {
                return codigo;
            }

            set
            {
                codigo = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public float Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public float Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public float Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }
    }
}
